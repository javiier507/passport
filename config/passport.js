var mongoose = require('mongoose'),
	User = mongoose.model('User');

module.exports = function(passport)
{
	passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    require('./passport-strategies/passport-local')(passport, User);
};