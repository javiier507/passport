var LocalStrategy   = require('passport-local').Strategy;

module.exports = function(passport, User)
{
    passport.use('local-login', new LocalStrategy({
        usernameField : 'usuario',
        passwordField : 'contrasena',
        passReqToCallback : true
    },
    function(req, usuario, contrasena, done) 
    {
        var body = {
            username: usuario,
            password: contrasena
        };

        User.findOne(body, function(err, user) 
        {
            if(err)
                return done(err);

            if(!user)
                return done(null, false, req.flash('loginMessage', 'Datos Incorrecto !')); 

            return done(null, user);
        });
    }));

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'usuario',
        passwordField : 'contrasena',
        passReqToCallback : true
    },
    function(req, usuario, contrasena, done) 
    {
        var newUser =  new User({
            name: req.body.nombre,
            lastname: req.body.apellido,
            username: usuario,
            email: req.body.correo,
            password: contrasena,
            image: req.body.imagen
        });

        console.log(newUser);

        User.findOne({$or:[{username: newUser.username}, {email: newUser.email}]}, function(err, user) 
        {
            if(err)
                return done(err);

            if(user)
            {
                return done(null, false, req.flash('registerMessage', 'Usuario / Correo Existente'));
            }
            else
            {

                newUser.save(function(err)
                {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            } 
        });
    }));
};