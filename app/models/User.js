var mongoose = require('mongoose'),
  	Schema = mongoose.Schema;

var UserSchema = new Schema({
  	name: String,
  	lastname: String,
  	username: String,
  	email: String,
  	password: String,
  	image: String
});

module.exports = mongoose.model('User', UserSchema);