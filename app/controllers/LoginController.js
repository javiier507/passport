var mongoose = require('mongoose'),
	User = mongoose.model('User');

module.exports = {
	index : function(request, response)
	{
		console.log('SESION --------->>>> '+request.user);
		response.render('index', {
			user : request.user
		});
	},

	getLogin: function(request, response)
	{
		var flash = request.flash('loginMessage'),
			loginMessage = flash.length>0 ? flash : undefined;
		console.log('FLASH --------->>>> '+loginMessage);

		response.render('login', {
			message: loginMessage 
		});		
	},

	getRegister: function(request, response)
	{
		var flash = request.flash('registerMessage'),
			registerMessage = flash.length>0 ? flash : undefined;
		console.log('FLASH --------->>>> '+registerMessage);

		response.render('register', {
			message: registerMessage 
		});	
	},

	profile: function(request, response)
	{
		response.render('profile', {
			user : request.user
		});
	}, 

	logout: function(request, response)
	{
		request.logout();
		response.redirect('/');
	}
};