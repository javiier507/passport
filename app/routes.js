var loginController = require('./controllers/LoginController');

module.exports = function(app, passport)
{
	app.get('/', loginController.index);

	app.route('/ingresar')
		.get(loginController.getLogin)
		.post(passport.authenticate('local-login', {
			successRedirect : '/perfil',
			failureRedirect : '/ingresar', 
			failureFlash : true
		}));

	app.route('/registrar')
		.get(loginController.getRegister)
		.post(passport.authenticate('local-signup', {
			successRedirect : '/perfil',
			failureRedirect : '/registrar', 
			failureFlash : true
		}));

	app.get('/perfil', isAuth, loginController.profile);
	app.get('/salir', loginController.logout);
};

function isAuth(request, response, next)
{
	if (request.isAuthenticated())
		return next();

	response.redirect('/');
}